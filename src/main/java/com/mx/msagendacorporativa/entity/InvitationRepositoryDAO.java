package com.mx.msagendacorporativa.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InvitationRepositoryDAO extends JpaRepository<InvitationEntity, String> {

	@Query(value="SELECT NEW com.mx.msagendacorporativa.entity.InvitationEntity(u.invitationId as invitationId , u.eventName as eventName, u.eventDate as eventDate, u.eventTime as eventTime, u.description as description, u.email as email, u.status as status, u.comments as comments ) FROM InvitationEntity u WHERE u.invitationId = :invitationId ")
	InvitationEntity getInvitationById(Integer invitationId);
	
	@Query(value = "SELECT SEQUENCE_INVITATION.nextval FROM dual", nativeQuery = true)
	public Integer getNextValLog();

}
