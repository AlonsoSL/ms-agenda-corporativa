package com.mx.msagendacorporativa.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;



public interface EventRepositoryDAO extends JpaRepository<EventEntity, String>{
	
	@Query(value="SELECT NEW com.mx.msagendacorporativa.entity.EventEntity(u.eventId as eventId , u.eventName as eventName, u.eventDate as eventDate, u.description as description, u.eventTime as eventTime ) FROM EventEntity u WHERE u.eventName LIKE :eventName ")
	EventEntity findEvent(String eventName);
	
	@Query(value = "SELECT SEQUENCE_EVENTS.nextval FROM dual", nativeQuery = true)
    public Integer getNextValLog();
	
	@Query(value="SELECT NEW com.mx.msagendacorporativa.entity.EventEntity(u.eventId as eventId , u.eventName as eventName, u.eventDate as eventDate, u.description as description, u.eventTime as eventTime ) FROM EventEntity u WHERE u.eventTime LIKE :eventTime AND u.eventDate LIKE :eventDate ")
	List<EventEntity> getEvents(String eventTime, String eventDate);
}
