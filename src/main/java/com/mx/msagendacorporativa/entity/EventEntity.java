package com.mx.msagendacorporativa.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "EVENTS")
public class EventEntity{


    @Id
    @Column(name = "EVENT_ID")
    private Integer eventId;
    
    @Column(name = "EVENT_NAME")
    private String eventName;
    
    @Column(name = "EVENT_DATE")
    private String eventDate;
    
    @Column(name = "DESCRIPTION")
    private String  description;
    
    @Column(name = "EVENT_TIME")
    private String  eventTime;

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public EventEntity() {
		super();
	}

	public EventEntity(Integer eventId, String eventName, String eventDate, String description, String eventTime) {
		super();
		this.eventId = eventId;
		this.eventName = eventName;
		this.eventDate = eventDate;
		this.description = description;
		this.eventTime = eventTime;
	}
    
}
