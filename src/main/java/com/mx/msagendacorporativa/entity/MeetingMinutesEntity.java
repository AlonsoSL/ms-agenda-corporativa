package com.mx.msagendacorporativa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "MEETING_MINUTES")
public class MeetingMinutesEntity {

	@Id
	@Column(name = "MEETING_MINUTES_ID")
	private Integer meetingMinutesId;

	@Column(name = "EVENT_ID")
	private Integer eventId;

	@Column(name = "SUMMARY")
	private String summary;

	@Column(name = "AGREEMENTS")
	private String agreements;

	public Integer getMeetingMinutesId() {
		return meetingMinutesId;
	}

	public void setMeetingMinutesId(Integer meetingMinutesId) {
		this.meetingMinutesId = meetingMinutesId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getAgreements() {
		return agreements;
	}

	public void setAgreements(String agreements) {
		this.agreements = agreements;
	}

	public MeetingMinutesEntity(Integer meetingMinutesId, Integer eventId, String summary, String agreements) {
		super();
		this.meetingMinutesId = meetingMinutesId;
		this.eventId = eventId;
		this.summary = summary;
		this.agreements = agreements;
	}

	public MeetingMinutesEntity() {
		super();
	}

}
