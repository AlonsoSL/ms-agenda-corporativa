package com.mx.msagendacorporativa.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TaskRepositoryDAO extends JpaRepository<TaskEntity, String> {

	@Query(value = "SELECT NEW com.mx.msagendacorporativa.entity.TaskEntity(u.taskId, u.taskName, u.eventId, u.description, u.assignedTo, u.email, u.status ) FROM TaskEntity u WHERE u.taskId = :taskId ")
	TaskEntity getTaskById(Integer taskId);
	
	@Query(value = "SELECT NEW com.mx.msagendacorporativa.entity.TaskEntity(u.taskId, u.taskName, u.eventId, u.description, u.assignedTo, u.email, u.status ) FROM TaskEntity u WHERE u.eventId = :eventId ")
	List<TaskEntity> getTasksByEvent(Integer eventId);

	@Query(value = "SELECT SEQUENCE_TASK.nextval FROM dual", nativeQuery = true)
	public Integer getNextValLog();

}
