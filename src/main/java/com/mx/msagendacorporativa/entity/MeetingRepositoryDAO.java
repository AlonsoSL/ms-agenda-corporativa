package com.mx.msagendacorporativa.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MeetingRepositoryDAO  extends JpaRepository<MeetingMinutesEntity, String>{

	@Query(value = "SELECT SEQUENCE_MEETING_MINUTES.nextval FROM dual", nativeQuery = true)
  public Integer getNextValLog();
}
