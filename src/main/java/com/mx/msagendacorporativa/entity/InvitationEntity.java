package com.mx.msagendacorporativa.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "INVITATION")
public class InvitationEntity{
	@Id
	@Column(name = "INVITATION_ID")
	private Integer invitationId;

	@Column(name = "EVENT_NAME")
	private String eventName;

	@Column(name = "EVENT_DATE")
	private String eventDate;

	@Column(name = "EVENT_TIME")
	private String eventTime;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "COMMENTS")
	private String comments;

	public Integer getInvitationId() {
		return invitationId;
	}

	public void setInvitationId(Integer invitationId) {
		this.invitationId = invitationId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public InvitationEntity(Integer invitationId, String eventName, String eventDate, String eventTime,
			String description, String email, String status, String comments) {
		super();
		this.invitationId = invitationId;
		this.eventName = eventName;
		this.eventDate = eventDate;
		this.eventTime = eventTime;
		this.description = description;
		this.email = email;
		this.status = status;
		this.comments = comments;
	}

	public InvitationEntity() {
		super();
	}

}
