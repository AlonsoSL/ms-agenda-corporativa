package com.mx.msagendacorporativa.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.msagendacorporativa.dto.ChangeTransitionStatusDTO;
import com.mx.msagendacorporativa.dto.CreateTaskDTO;
import com.mx.msagendacorporativa.dto.TaskDTO;
import com.mx.msagendacorporativa.service.TaskService;

@RestController
@CrossOrigin("*")
@RequestMapping("/task")
public class TaskController {

	@Autowired
	TaskService taskService;

	@PostMapping("/newTask")
	public ResponseEntity<String> newTask(@RequestBody() CreateTaskDTO dto) {
		try {
			taskService.createTask(dto);
			return new ResponseEntity<>("tarea Creada", HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}
	
    @PutMapping("/transitionSatus")
    public ResponseEntity<String> changetransitionStatus(@RequestBody() ChangeTransitionStatusDTO dto){
    	try {
    		taskService.changeTransitionStatus(dto);
            return new ResponseEntity<>("", HttpStatus.OK);
    	}catch (Exception e) {
			System.out.println("fallo");
			return new ResponseEntity<>(""+e.getMessage(),HttpStatus.BAD_REQUEST );
		}

    }


	@GetMapping("/tasksByEvent")
	public ResponseEntity<Object> getTaskByEventId(Integer eventId) {
		try {
			List<TaskDTO> result =new ArrayList<>();
			result = taskService.getTasksByEvent(eventId);
			return new ResponseEntity<>(result, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}


}
