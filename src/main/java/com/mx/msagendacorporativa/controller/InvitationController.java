package com.mx.msagendacorporativa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.msagendacorporativa.dto.AcceptRejectInvitationDTO;
import com.mx.msagendacorporativa.dto.SendInvitationDTO;
import com.mx.msagendacorporativa.service.InvitationService;

@RestController
@CrossOrigin("*")
@RequestMapping("/invitation")
public class InvitationController {

	@Autowired
	InvitationService invitationService;
	
    @PostMapping("/sendInvitation")
    public ResponseEntity<String> sendInvitation(@RequestBody() SendInvitationDTO dto){
    	invitationService.sendInvitaion(dto);
        return new ResponseEntity<>("Invitacion enviada", HttpStatus.OK);
    }
    
    @PutMapping("/AcceptOrReject")
    public ResponseEntity<String> acceptOrReject(@RequestBody() AcceptRejectInvitationDTO dto){
    	try {
        	invitationService.acceptRejectInvitaion(dto);
            return new ResponseEntity<>("", HttpStatus.OK);
    	}catch (Exception e) {
			System.out.println("fallo");
			return new ResponseEntity<>(""+e.getMessage(),HttpStatus.BAD_REQUEST );
		}

    }
}
