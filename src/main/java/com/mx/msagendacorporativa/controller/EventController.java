package com.mx.msagendacorporativa.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.msagendacorporativa.dto.CreateEventDTO;
import com.mx.msagendacorporativa.dto.EventDTO;
import com.mx.msagendacorporativa.service.EventService;

@RestController
@CrossOrigin("*")
@RequestMapping("/event")
public class EventController {
	@Autowired
	EventService eventService;

	@GetMapping("/status")
	public String status() {
		System.out.println("LLEGO AL CONTROLLER");
		return "STATUS OK";
	}

	@GetMapping("/getEvent")
	public ResponseEntity<Object> getEventByName(String eventName) {
		try {
			EventDTO result = new EventDTO();
			result = eventService.getEventByName(eventName);
			return new ResponseEntity<>(result, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping("/newEvent")
	public ResponseEntity<String> newEvent(@RequestBody() CreateEventDTO dto) {
		try {
			eventService.createEvent(dto);
			return new ResponseEntity<>("evento Creado", HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

}
