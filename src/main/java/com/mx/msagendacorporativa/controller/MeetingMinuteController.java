package com.mx.msagendacorporativa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.msagendacorporativa.dto.CreateMeetingMinuteDTO;
import com.mx.msagendacorporativa.service.MeetingMinuteService;

@RestController
@CrossOrigin("*")
@RequestMapping("/mettingMinutes")
public class MeetingMinuteController {
	
	@Autowired
	MeetingMinuteService mettingMinuteService;

    @PostMapping("/crearMinuta")
    public ResponseEntity<String> newEvent(@RequestBody() CreateMeetingMinuteDTO dto){
    	mettingMinuteService.createMettingMinutes(dto);
        return new ResponseEntity<>("minuta Creada", HttpStatus.OK);
    }
}
