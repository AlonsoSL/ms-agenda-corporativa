package com.mx.msagendacorporativa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.msagendacorporativa.dto.CreateMeetingMinuteDTO;
import com.mx.msagendacorporativa.entity.MeetingMinutesEntity;
import com.mx.msagendacorporativa.entity.MeetingRepositoryDAO;

@Service
public class MeetingMinuteServiceImpl implements MeetingMinuteService {

	@Autowired
	MeetingRepositoryDAO meetingRepositoryDAO;

	@Override
	public String createMettingMinutes(CreateMeetingMinuteDTO dto) {
		Integer nextSequenceValLog = meetingRepositoryDAO.getNextValLog();

		MeetingMinutesEntity entity = new MeetingMinutesEntity();
		entity.setEventId(nextSequenceValLog);
		entity.setEventId(dto.getEventId());
		entity.setSummary(dto.getSummary());
		entity.setAgreements(dto.getAgreements());

		meetingRepositoryDAO.save(entity);
		return "CREADO";
	}
}
