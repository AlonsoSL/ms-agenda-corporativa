package com.mx.msagendacorporativa.service;

import com.mx.msagendacorporativa.dto.AcceptRejectInvitationDTO;
import com.mx.msagendacorporativa.dto.SendInvitationDTO;

public interface InvitationService {

	public String sendInvitaion(SendInvitationDTO dto);
	public void acceptRejectInvitaion(AcceptRejectInvitationDTO dto);
}
