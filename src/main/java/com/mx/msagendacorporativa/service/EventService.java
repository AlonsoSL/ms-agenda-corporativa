package com.mx.msagendacorporativa.service;

import com.mx.msagendacorporativa.dto.CreateEventDTO;
import com.mx.msagendacorporativa.dto.EventDTO;

public interface EventService {
	
	public EventDTO getEventByName(String eventName);
	public String createEvent(CreateEventDTO dto);
}
