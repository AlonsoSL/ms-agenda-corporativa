package com.mx.msagendacorporativa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.msagendacorporativa.dto.CreateEventDTO;
import com.mx.msagendacorporativa.dto.EventDTO;
import com.mx.msagendacorporativa.entity.EventEntity;
import com.mx.msagendacorporativa.entity.EventRepositoryDAO;
import com.mx.msagendacorporativa.exception.NotFoundException;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	EventRepositoryDAO eventRepositoryDAO;

	@Override
	public String createEvent(CreateEventDTO dto) {
		Integer nextSequenceValLog = eventRepositoryDAO.getNextValLog();
		
		List<EventEntity> events= eventRepositoryDAO.getEvents(dto.getEventTime(), dto.getEventDate());
		if(events.size()>= 1) {
			throw new NotFoundException("ya existen Eventos programados para esa fecha y hora");	
		}

		EventEntity entity = new EventEntity();
		entity.setEventId(nextSequenceValLog);
		entity.setEventName(dto.getEventName());
		entity.setEventDate(dto.getEventDate());
		entity.setDescription(dto.getDescription());
		entity.setEventTime(dto.getEventTime());

		eventRepositoryDAO.save(entity);
		return "CREADO";
	}

	@Override
	public EventDTO getEventByName(String eventName) {
		EventEntity event = new EventEntity();
		event = eventRepositoryDAO.findEvent(eventName);

		if (event == null) {
			throw new NotFoundException("event not foud");
		}

		EventDTO dto = new EventDTO();
		dto.setEventName(event.getEventName());
		dto.setEventDate(event.getEventDate());
		dto.setDescription(event.getDescription());
		return dto;
	}

}
