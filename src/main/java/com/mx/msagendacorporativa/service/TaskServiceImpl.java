package com.mx.msagendacorporativa.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.msagendacorporativa.dto.ChangeTransitionStatusDTO;
import com.mx.msagendacorporativa.dto.CreateTaskDTO;
import com.mx.msagendacorporativa.dto.TaskDTO;
import com.mx.msagendacorporativa.entity.TaskEntity;
import com.mx.msagendacorporativa.entity.TaskRepositoryDAO;
import com.mx.msagendacorporativa.exception.NotFoundException;

@Service
public class TaskServiceImpl implements  TaskService{

	@Autowired
	TaskRepositoryDAO taskRepositoryDAO;
	
	@Override
	public void createTask(CreateTaskDTO dto) {
		Integer nextSequenceValLog = taskRepositoryDAO.getNextValLog();

		TaskEntity entity = new TaskEntity();
		entity.setTaskId(nextSequenceValLog);
		entity.setTaskName(dto.getTaskName());
		entity.setEventId(dto.getEventId());
		entity.setDescription(dto.getDescription());
		entity.setAssignedTo(dto.getAssignedTo());
		entity.setEmail(dto.getEmail());

		taskRepositoryDAO.save(entity);
		
	}

	@Override
	public void changeTransitionStatus(ChangeTransitionStatusDTO dto) {
		TaskEntity entity = taskRepositoryDAO.getTaskById(dto.getTaskId());
		
		if (entity == null) {
			throw new NotFoundException("task not foud");
		}
		System.out.println("RECUPERO INFORMACION ************** "+ entity.getAssignedTo());
		entity.setStatus(dto.getStatus());
	
		taskRepositoryDAO.save(entity);
		
	}

	@Override
	public List<TaskDTO> getTasksByEvent(Integer eventId) {
		List<TaskEntity> result = new ArrayList<>();
		result = taskRepositoryDAO.getTasksByEvent(eventId);
		
        if (result.size()==0 || result.isEmpty()) {
            throw new NotFoundException("No se encontron tareas asosiadas al evento");
        }
        
        List<TaskDTO> dto = new ArrayList<TaskDTO>();
        for (TaskEntity task: result) {
        	dto.add(new TaskDTO(task.getTaskId(), task.getTaskName(), task.getEventId(), task.getDescription(), task.getAssignedTo(), task.getEmail(), task.getStatus()));
        }
        return dto;
	}


}
