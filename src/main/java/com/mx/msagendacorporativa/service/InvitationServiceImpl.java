package com.mx.msagendacorporativa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.msagendacorporativa.dto.AcceptRejectInvitationDTO;
import com.mx.msagendacorporativa.dto.SendInvitationDTO;
import com.mx.msagendacorporativa.entity.InvitationEntity;
import com.mx.msagendacorporativa.entity.InvitationRepositoryDAO;
import com.mx.msagendacorporativa.exception.NotFoundException;

@Service
public class InvitationServiceImpl implements InvitationService{

	@Autowired
	InvitationRepositoryDAO invitationRepositoryDAO;
	
	@Override
	public String sendInvitaion(SendInvitationDTO dto) {
		Integer nextSequenceValLog = invitationRepositoryDAO.getNextValLog();
		
		InvitationEntity entity = new InvitationEntity();
		entity.setInvitationId(nextSequenceValLog);
		entity.setEventName(dto.getEventName());
		entity.setEventDate(dto.getEventDate());
		entity.setEventTime(dto.getEventTime());
		entity.setDescription(dto.getDescription());
		entity.setEmail(dto.getEmail());
		entity.setStatus("ENVIADA");
		
		invitationRepositoryDAO.save(entity);
		return "CREADO";
	}
	
	@Override
	public void acceptRejectInvitaion(AcceptRejectInvitationDTO dto) {
		InvitationEntity entity = invitationRepositoryDAO.getInvitationById(dto.getInvitationId());
		
    	if(dto.getStatus().equals("RECHAZADA") && dto.getComments().isEmpty()) {
    		throw new NotFoundException("es necesario agregar argumentos para rechazar la invitacion");
    	}
		System.out.println("RECUPERO INFORMACION ************** "+ entity.getEventName());
		System.out.println("COMENTARIOS ************** "+ dto.getComments());
		
		entity.setStatus(dto.getStatus());
		entity.setComments(dto.getComments());
	
		
		invitationRepositoryDAO.save(entity);
	}
}
