package com.mx.msagendacorporativa.service;

import java.util.List;

import com.mx.msagendacorporativa.dto.ChangeTransitionStatusDTO;
import com.mx.msagendacorporativa.dto.CreateTaskDTO;
import com.mx.msagendacorporativa.dto.TaskDTO;

public interface TaskService {
	
	public void createTask(CreateTaskDTO dto);
	public void changeTransitionStatus(ChangeTransitionStatusDTO dto);
	public List<TaskDTO> getTasksByEvent(Integer eventId);
}
