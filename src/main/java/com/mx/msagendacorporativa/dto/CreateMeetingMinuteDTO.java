package com.mx.msagendacorporativa.dto;

public class CreateMeetingMinuteDTO {

	private Integer meetingMinutesId;
	private Integer eventId;
	private String summary;
	private String agreements;
	
	public Integer getMeetingMinutesId() {
		return meetingMinutesId;
	}
	public void setMeetingMinutesId(Integer meettingMinutesId) {
		this.meetingMinutesId = meettingMinutesId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getAgreements() {
		return agreements;
	}
	public void setAgreements(String agreements) {
		this.agreements = agreements;
	}
	public CreateMeetingMinuteDTO(Integer meetingMinutesId, Integer eventId, String summary, String agreements) {
		super();
		this.meetingMinutesId = meetingMinutesId;
		this.eventId = eventId;
		this.summary = summary;
		this.agreements = agreements;
	}
	public CreateMeetingMinuteDTO() {
		super();
	}
	
}
