package com.mx.msagendacorporativa.dto;

public class MessageDTO {

	private String message;
	private String estatus;
	


	public MessageDTO() {
		super();
	}

	public MessageDTO(String message, String estatus) {
		super();
		this.message = message;
		this.estatus = estatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}
