package com.mx.msagendacorporativa.dto;

public class AcceptRejectInvitationDTO {

	private Integer invitationId;
	private String status;
	private String comments;

	public Integer getInvitationId() {
		return invitationId;
	}

	public void setInvitationId(Integer invitationId) {
		this.invitationId = invitationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public AcceptRejectInvitationDTO() {
		super();
	}

	public AcceptRejectInvitationDTO(Integer invitationId, String status, String comments) {
		super();
		this.invitationId = invitationId;
		this.status = status;
		this.comments = comments;
	}

}
