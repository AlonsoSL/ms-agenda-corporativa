package com.mx.msagendacorporativa.dto;

public class CreateEventDTO {

	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String description;
	private String eventTime;

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public CreateEventDTO(Integer eventId, String eventName, String eventDate, String description, String eventTime) {
		super();
		this.eventId = eventId;
		this.eventName = eventName;
		this.eventDate = eventDate;
		this.description = description;
		this.eventTime = eventTime;
	}

	public CreateEventDTO() {
		super();
	}

}
