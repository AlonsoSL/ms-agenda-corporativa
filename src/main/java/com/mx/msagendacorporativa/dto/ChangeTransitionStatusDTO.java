package com.mx.msagendacorporativa.dto;

public class ChangeTransitionStatusDTO {

	private Integer taskId;
	private String status;
	
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public ChangeTransitionStatusDTO(Integer taskId, String status) {
		super();
		this.taskId = taskId;
		this.status = status;
	}
	public ChangeTransitionStatusDTO() {
		super();
	}
	
}
