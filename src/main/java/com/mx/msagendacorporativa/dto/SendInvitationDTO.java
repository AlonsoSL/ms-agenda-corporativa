package com.mx.msagendacorporativa.dto;

public class SendInvitationDTO {

	private Integer invitationId;
	private String eventName;
	private String eventDate;
	private String eventTime;
	private String description;
	private String email;
	public Integer getInvitationId() {
		return invitationId;
	}
	public void setInvitationId(Integer invitationId) {
		this.invitationId = invitationId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public SendInvitationDTO(Integer invitationId, String eventName, String eventDate, String eventTime,
			String description, String email) {
		super();
		this.invitationId = invitationId;
		this.eventName = eventName;
		this.eventDate = eventDate;
		this.eventTime = eventTime;
		this.description = description;
		this.email = email;
	}
	public SendInvitationDTO() {
		super();
	}
	
	
}
