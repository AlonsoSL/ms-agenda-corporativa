package com.mx.msagendacorporativa.dto;

public class CreateTaskDTO {

	private Integer taskId;
	private String taskName;
	private Integer eventId;
	private String description;
	private String assignedTo;
	private String email;

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CreateTaskDTO(Integer taskId, String taskName, Integer eventId, String description, String assignedTo,
			String email) {
		super();
		this.taskId = taskId;
		this.taskName = taskName;
		this.eventId = eventId;
		this.description = description;
		this.assignedTo = assignedTo;
		this.email = email;
	}

	public CreateTaskDTO() {
		super();
	}
}
