package com.mx.msagendacorporativa.dto;

public class EventDTO {

    private String eventName;
    private String eventDate;
    private String description;
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public EventDTO(String eventName, String eventDate, String description) {
		super();
		this.eventName = eventName;
		this.eventDate = eventDate;
		this.description = description;
	}
	public EventDTO() {
		super();
	}
}
