package com.mx.msagendacorporativa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsAgendaCorporativaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsAgendaCorporativaApplication.class, args);
	}

}
